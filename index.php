<?php

/////////////////////////////////////////
// @ Example usage of the HK API
// @ 15th February 2013
// @ Author: Ashley Ford
// @ ashley@harkable.com
////////////////////////////////////////

include('class/HkCompAPI.class.php');

$competition = new HkCompAPI();

    if(isset($_POST['_submit'])){
    
      //$_POST['dob'] = $_POST['_year'].'-'.$_POST['_month'].'-'.$_POST['_day'];
    
      $validation = array(
  
        //@ Format of the array is as follows:
        //@ Input Value Name
        //@ Error Message
        //@ Required, valid_email, over_18, captcha (value must be blank)     
        
      array('fname',    "Please enter your first name",          'required'),
      array('lname',    "Please enter your last name",           'required'),
      //array('dob',      "This promotion is open to users over 18!",  'over_18'),
      array('email',    "Please enter a valid email address",      'valid_email'),
      //array('terms',    "Please Accept the Terms &amp; Conditions",  'required'),
      array('subscribe',  "Please Accept the Terms &amp; Conditions",  'required'),
      array('_captcha', "We're not sure you're a human...",      'captcha')
  
    );  
    
    // validate for errors
    $errors = $competition->validate($validation);
    
    // validate returns FALSE if no errors are found
    if($errors===FALSE){
      // save the data via the api call with the campaign name
          $save = $competition->save('ign_evil');
                    
          if(isset($save['_auth']) && $save['_auth']=='failed'){
            echo 'Error Authenticating With the API';           
          }
             
            // if the data was saved successfully   
          if(isset($save['_saved'])==TRUE){
          
          //redirect to thankyou page
            
          header('location: thankyou.php');

          }
                     
        }
                      
    } 
?>


<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>The Evil Within - Vote for your favourite horror moive in association with IGN</title>

    <link rel="shortcut icon" href="favicon.ico">
    <link rel=apple-touch-icon href="apple-touch-icon.png">
    <link rel=apple-touch-icon sizes=72x72 href="apple-touch-icon-72x72.png">
    <link rel=apple-touch-icon sizes=114x114 href="apple-touch-icon-114x114.png">

    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
     <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
     <script type="text/javascript" src="//use.typekit.net/nwm4qph.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>


  <script src="js/hide_it.js"></script>

  </head>


<body>


 <script src="js/output_age_form.js"></script>

<!--uncomment for live-->
<div id="policyNotice" style="display:none">
    <div class="close-btn">
        <a href="#_" title="Close" data-domain=".ign.com">Close</a>
    </div>
    <p>We use cookies and other technologies to improve your online experience. By using this Site, you consent to this use as described in our <a href="http://corp.ign.com/policies/cookie-eu" target="_blank">Cookie Policy</a>.</p>
</div>
<!--uncomment for live-->

    <!--off canvas menu-->
  <div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
      <nav class="tab-bar top" data-topbar role="navigation">

          <section class="left-small show-for-small-only">
              <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
          </section>

          <div class="row">

            <div class="ign_red-wrapper ">
              <h1 class="title"><a href="http://ign.com"><img src="img/ign-logo.png" alt="IGN logo"></a></h1>
            </div>

            <ul class="left share_btn" id="share_override">
              <li><a href="#"></a></li>
            </ul>

          </div>

      </nav>

      <aside class="left-off-canvas-menu show-for-small-only">
        <ul class="off-canvas-list">
          <li><label>The Evil Within</label></li>
              <li><a href="#_" class="choose">Choose your film</a></li>
              <li><a href="#_" class="join">Join the screening</a></li>
              <li><a href="#_" class="about">about the game</a></li>
              <li><a href="http://theevilwithin.com/preorder" target="_blank" class="preorder" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in navigation']);">pre-order the game</a></li>
        </ul>
      </aside>
      <!--end off canvas menu-->

      <header>
        <div class="secondary nav" id="stick">
          <ul>
              <li><a href="#_" class="choose">Choose your film</a></li>
              <li><a href="#_" class="join">Join the screening</a></li>
              <li><a href="#_" class="about">about the game</a></li>
              <li><a href="http://theevilwithin.com/preorder" target="_blank" class="preorder" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in navigation']);">pre-order the game</a></li>
          </ul>
        </div>
        <div class="header_wrapper row">
          <div class="large-4 medium-4 small-12 columns hide-for-small-only">
            <h1>Resurrect a Classic.<br>
  <br>
Vote for your favourite scary movie and we'll show the winner at a special screening at Vue Westfield.</h1>
          </div>
          <div class="large-4 medium-4 small-12 columns hide-for-small-only">
            <img class="evilLogo" src="img/evil_logo.png" alt="Evil within logo">
          </div>

          <!--mark-up order change for mobile-->
          <div class="large-4 medium-4 small-12 columns show-for-small-only">
            <img class="evilLogo" src="img/evil_logo.png" alt="evil within logo">
          </div>
          <div class="large-4 medium-4 small-12 columns show-for-small-only">
            <h1>Resurrect a Classic.<br>
  <br>
Vote for your favourite scary movie and we'll show the winner at a special screening at Vue Westfield.</h1>
          </div>
          <!--end markup order change-->

          <div class="large-4 medium-4 hide-for-small-only columns">
            <div class="pre-order_wrapper">
              <a href="http://theevilwithin.com/preorder" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in hero']);">PRE-ORDER NOW <span></span></a>
            </div>
          </div>
        </div>
      </header>

      <div class="aboutthegame">
          <div class="row">
            <div class="large-8 columns">
              <h2>The Evil Within</h2>
              <p>From Shinji Mikami - father of the Resident Evil series - and Tango Gameworks, The Evil Within reaffirms the meaning of pure survival horror. Detailed environments, anxiety, and an intricate story are combined to create an immersive world that will leave you in a state of tension and panic. With limited resources at your disposal, you’ll fight for survival and experience fear in a blend of horror and action.
<br>
While investigating the scene of a gruesome mass murder, Detective Sebastian Castellanos and his partners encounter a mysterious and powerful force. After seeing the slaughter of fellow officers, Sebastian is ambushed and knocked unconscious. When he awakens, he finds himself in a deranged world where hideous creatures wander among the dead.
 <br>
Facing unimaginable terror, and fighting for survival, Sebastian embarks on a journey to unravel what’s behind this evil force…</p>
            </div>
            <div class="large-4 columns">
              <div class="flex-video">
                      <iframe width="560" height="315" src="http://evilwith.in/TGS2014" frameborder="0" allowfullscreen></iframe>
              </div>
          </div>
        </div>
      </div>

      <div ng-app="EvilApp">
        <section class="main-section" id="movies" ng-controller="AppCtrl as app">
          <div class="row" data-equalizer>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/0ae0e3c32f6327941d5574fe23d0f26828d780.jpg" alt="Pans Labyrinth">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="pans_labyrinth">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/600full-peeping-tom-poster.jpg" alt="Peeping Tom">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="peeping_tom">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/a67420zu7hz.jpg" alt="Amityville Horror">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="amityville_horror">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/130427025326187850.jpg" alt="The Fog">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="the_fog">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
          </div>

          <div class="row" data-equalizer>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/carrie2013-international-poster.jpg" alt="Carrie">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="carrie">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/dont-look-now.11272.jpg" alt="">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="dont_look_now">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/download-copy.jpg" alt="The Shining">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="the_shining">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/Evil_Dead_II_poster.jpg" alt="Evil Dead 2">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="evil_dead_2">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
          </div>


         <div class="row" data-equalizer>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/para-activity-poster.jpg" alt="Paranormal Activity">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="paranormal_activity">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/ringu.jpg" alt="Ringu">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="ringu">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/rosemarys-baby-poster.jpg" alt="Rosemary's Baby">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="rosemarys_baby">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/screamposter03.jpg" alt="Scream">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="scream">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
          </div>

          <div class="row" data-equalizer>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
               <div class="blud_wrapper">
                <img src="img/posters/flymovieposter1986.jpg" alt="The Fly">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="the_fly">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/silence-of-the-lamb-poster.jpg" alt="Silence of the Lambs">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="silence_of_the_lambs">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/TheWickerMan-poster.jpg" alt="The Wicker Man">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="the_wicker_man">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
            <div class="large-3 medium-6 small-12 columns" data-equalizer-watch>
              <div class="blud_wrapper">
                <img src="img/posters/Omen_ver4.jpg" alt="The Omen">
              </div>
              <div class="voting_panel">
                <p vote-directive activate="selected" data-title="the_omen">Vote</p>
                <div class="check"><span></span></div>
              </div>
            </div>
          </div>



            <div class="row">
                <div class="large-6 medium-8 small-10 large-centered medium-centered small-centered columns" id="submit"  data-reveal-id="confirm_modal">
                  <a href="#" class="button">Submit your vote</a>
                </div>
            </div>
            <div class="row">
              <div class="large-8 large-centered columns">
                <p class="results">RESULTS WILL BE RELEASED ON 01/10</p>
              </div>
            </div>


        </section>
      </div>

        <section class="comp" id="comp">


          <?php 

                  if(isset($errors)){
                      // if there were errors in form processing show them here
                      echo '<div class="alert alert-error"><ul>';                  
                          foreach($errors as $error){           
                            echo '<li class="alert-box alert round">'.$error.'</li>';  
                          }         
                      echo '</ul></div>';
                  
                  }
          ?>

          <div class="comp_wrapper row">
            <div class="large-5 columns">
              <h2>Enter your details to be notified when ticket booking goes live.</h2>

               <p>We’ll be screening the film with the most votes in 
                Vue Westfield on October 9th, tickets will be available on a first come, first served basis so you’ll need to be quick!</p>

                <p>If you want to receive an email when tickets are available to book, please enter it in the box on the right.</p>

                <p>Note that you will only be eligible to attend the event if you are 18 years of age or over and a UK resident. Photo ID will be required on the night.</p>
            </div>
            <div class="large-6 large-offset-1 columns">


                <form method="post" class="form-horizontal">
                  <h2>Your Details</h2>
                  <div class="row">
                    <div class="large-6 columns">
                      <label>FIRST NAME
                       
                        <input type="text" name="fname" value="<?php if(isset($_POST['fname'])){echo $_POST['fname'];}?>"> 
                      </label>
                    </div>
                    <div class="large-6 columns">
                      <label>LAST NAME
                        <input type="text" name="lname" value="<?php if(isset($_POST['lname'])){echo $_POST['lname'];}?>">
                      </label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <label>EMAIL
                        <input type="text" name="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>"> 
                      </label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-12 columns">
                      <div class="row pushDown">
 <!--                        <div class="large-1 column">
                          <input id="checkbox1" type="checkbox" name="terms" value="1">
                        </div>
                        <div class="large-11 column">
                          <label class="checkboxLabel" for="checkbox1">I confirm that I am a UK resident over the age of 18 and have read and understood the <a href="#_">terms and conditions and the privacy policy.</a></label>
                        </div> -->
                      </div>

                        <div class="row">
                          <div class="large-1 medium-1 small-1 column">
                            <input id="checkbox2" type="checkbox" name="subscribe" value="1">
                          </div>
                          <div class="large-11 medium-11 small-11 column">
                            <label class="checkboxLabel" for="checkbox2">I agree that IGN can use my email address to send me a notification when tickets for the screening event are available to be booked.</label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="large-12 columns">
                        <input type="hidden" name="_captcha" value=""> 
                        <input type="submit" value="SUBMIT" class="button" name="_submit" ng-submit>
                      </div>
                    </div>
                  <!-- </div> -->
                </form>
            </div>
          </div>
        </section>

        <!--start logo garden-->
        <div class="logo_garden">
          <div class="row">
            <div class="large-12">
              <img src="img/logo_garden.jpg" alt="logos">
              <p class="sml_text">© 2014 ZeniMax Media Inc. Developed in association with Tango Gameworks. The Evil Within, Tango, Tango Gameworks, Bethesda, Bethesda Softworks, ZeniMax and related logos are registered trademarks or trademarks of ZeniMax Media Inc. in the U.S. and/or other countries. All other trademarks or trade names are the property of their respective owners. All Rights Reserved.</p>
            </div>
          </div>
        </div>
        <!--end logo garden-->

        <footer class="tab-bar">
          <div class="row">

            <div class="ign_red-wrapper">
              <div class="title"><img src="img/ign_ent.png" alt="IGN Entertainment logo"></div>

              <ul>
                <li><a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy policy</a></li>
                <li><a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User agreement</a></li>
              </ul>
            </div>

            <ul class="left links">
              <ul>
                <li><p>Copyright 2014<br>IGN Entertainment UK, Inc.</p></li>
                <li><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a></li>
                <li><a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a></li>
                <li><a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></li>
              </ul>
            </ul>

          </div>
        </footer>

      <a class="exit-off-canvas"></a>

    </div>
  </div>


<div id="start_modal" class="reveal-modal small">
    <p>Please confirm you are 18 years of age or over, if you are not please <a href="http://ign.com">go back to IGN</a></p>
</div>

<div id="confirm_modal" class="reveal-modal small comp" data-reveal>
  <h2>Thanks.</h2>
  <p class="lead">Your vote has been counted.</p>
  <a class="close-reveal-modal">&#215;</a>
</div>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js"></script>

    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/share.min.js"></script>
    <script src="js/ageChecker.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/waypoints-sticky.min.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/vote.js"></script>

    <script>

      (function(){
          var cookieName = 'persist-policy';
          var cookieValue = 'eu';
          var createCookie = function(name,value,days,domain) {
              var expires = '';
              var verifiedDomain = '';
              if (days) {
                  var date = new Date();
                  date.setTime(date.getTime()+(days*24*60*60*1000));
                  expires = '; expires='+date.toGMTString();
              }
              if (domain) {
                  verifiedDomain = '; domain='+domain;
              }
              document.cookie = name+'='+value+expires+verifiedDomain+'; path=/';
          };
          var readCookie = function(name) {
              var nameEQ = name + "=";
              var ca = document.cookie.split(';');
              for(var i=0;i < ca.length;i++) {
                  var c = ca[i];
                  while (c.charAt(0)==' ') c = c.substring(1,c.length);
                  if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
              }
              return null;
          };

          window.initPolicyWidget = function(){
              jQuery('#policyNotice').show().find('.close-btn a').click(function(e){
                  createCookie(cookieName, cookieValue, 180, jQuery(this).data('domain'));
                  jQuery('#policyNotice').remove();
                  return false;
              });
          }
          var cookieContent = readCookie(cookieName);
          if (typeof  cookieContent === 'undefined' || cookieContent != cookieValue) {
              jQuery(document).ready(initPolicyWidget);
              jQuery(document).trigger('policyWidgetReady');
          }
      })();

  </script>


  <!--begin GA script-->
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-15279170-1']);
    _gaq.push(['_trackPageview', 'evil_within_tracking']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
  <!--end GA script-->

  <!-- Begin comScore Tag -->
  <!--script>
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "3000068" });
    (function() {
      var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
      s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
      el.parentNode.insertBefore(s, el);
    })();
  </script>
  <noscript>
    <img src="http://b.scorecardresearch.com/p?c1=2&c2=3000068&cv=2.0&cj=1" />
  </noscript-->
   <!-- End comScore Tag -->

  </body>
</html>
