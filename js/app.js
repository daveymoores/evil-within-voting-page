// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){

	$('#stick').waypoint('sticky');

	$(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
	  $(window).scrollTo('#comp', 800, 'swing');
	});

	function makeActive(target){
		var el = target;

		el.parent().parent().find('li').each(function(){
			if($(this).find('a').hasClass('active')) {
				$(this).find('a').removeClass('active');
			}
		});

		el.addClass('active');
	}

	$('.menu-icon').on('click', function(){
		$('.off-canvas-wrap').foundation('offcanvas', 'toggle', 'move-right');
	});

	$('.choose').on('click', function(){

		makeActive($(this));
		$(window).scrollTo('#movies', 500, 'swing');
		$('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');

	});

	$('.join').on('click', function(){
		makeActive($(this));
		$(window).scrollTo('#comp', 500, 'swing');
		$('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');

	});

	$('.about').on('click', function(){
		makeActive($(this));
		$(window).scrollTo('.aboutthegame', 500, 'swing');
		$('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');

	});

	if($('#no_age_cookie').length) {
		$('#no_age_cookie').appendTo('#start_modal').css('visibility', 'visible');
		$('#start_modal').foundation('reveal', 'open');
		$('.chosen-container').css('width', '31.5%');
	}


	new Share(".share_btn", {
		ui: {
		  flyout: "bottom center" 
		},
		networks: {
		    google_plus: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/theevilwithin/"
		    },
		    twitter: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/theevilwithin/",
		      description:    "Vote for your favourite horror movie so @IGNUK and @TheEvilWithin can put on a special screening for the winning film!"
		    },
		    facebook: {
		      enabled: true,
		      //load_sdk: // Load the FB SDK. If false, it will default to Facebook's sharer.php implementation. 
		                // NOTE: This will disable the ability to dynamically set values and rely directly on applicable Open Graph tags.
		                // [Default: true]
		      url: "uk-microsites.ign.com/theevilwithin/",
		      app_id: "551643344962507",
		      title: "Vote for your favourite horror movie with IGN and The Evil Within",
		      caption: "Vote for your favourite horror movie with IGN and The Evil Within",
		      description:    "Vote for your favourite horror movie so @IGNUK and @TheEvilWithin can put on a special screening for the winning film!"
		      //image: // image to be shared to Facebook [Default: config.image]
		    },
		    pinterest: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/theevilwithin/",
		      //image:   // image to be shared to Pinterest [Default: config.image]
		      description:    "Vote for your favourite horror movie so @IGNUK and @TheEvilWithin can put on a special screening for the winning film!"
		    },
		    email: {
		      enabled: true,
		      title:     "Vote for your favourite horror movie with IGN and The Evil Within",
		      description:    "Vote for your favourite horror movie so @IGNUK and @TheEvilWithin can put on a special screening for the winning film!"
		    }
		  }

	});
});



/**angular**/

var evilapp = angular.module('EvilApp', []);

evilapp.controller('AppCtrl', function($scope){
	var app = this;

	app.body = $scope;

	app.message = "Vote";
	app.class = "inactive";



});

evilapp.directive('voteDirective', function(){
	return function (scope, element, attrs) { //grab the element

		restrict: "A",

		element.on('click', function(){

			if (element.hasClass('dontClick')) {
		        return false;
		     }

			//if($('#movies').hasClass('active') !== true) {

				if(element.hasClass(attrs.activate) !== true) {
					element.text(attrs.activate).addClass(attrs.activate);
					element.text(attrs.activate).next().addClass(attrs.activate);


					$('#movies').addClass('active');

					//if one hasnt been selected
					$('#movies').find('.large-3').each(function(){
						if($(this).find('p').hasClass('selected') !== true) {

							$(this).animate({
								opacity: 0.3 
							}, 400, 'swing', function(){
								//$(window).scrollTo('#submit', 800, 'swing', {offset: -100});
								$(this).find('p').addClass('dontClick');
							});

						} else {
							//element.removeClass(attrs.activate).text(scope.app.message);
							
						}
					});


				} else {

					element.removeClass(attrs.activate).text(scope.app.message);
					element.next().removeClass(attrs.activate);

					//if one has been selected
					$('#movies').find('.large-3').each(function(){
							$(this).animate({
								opacity: 1
							}, 400, 'swing', function(){
								//$(window).scrollTo('#submit', 800, 'swing', {offset:-100});
								$(this).find('p').removeClass('dontClick');
							});
					});
				}

			//}
		
		});

	}
});



/**end angular**/